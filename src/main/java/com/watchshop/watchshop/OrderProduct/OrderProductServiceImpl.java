package com.watchshop.watchshop.OrderProduct;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderProductServiceImpl implements OrderProductService {
  
  private OrderProductRepository orderproductRepository;

  @Autowired
  public OrderProductServiceImpl(OrderProductRepository theorderproductRepository) {
    orderproductRepository = theorderproductRepository;
  }

  @Override
  public List<OrderProduct> findAll() {
    return orderproductRepository.findAll(); 
  }

  @Override
  public OrderProduct findById(int id) {
    Optional<OrderProduct> result = orderproductRepository.findById(id);
    
    OrderProduct orderproduct = null;
    
    if (result.isPresent()) {
      orderproduct = result.get();
    } else {
      throw new RuntimeException("Did not find OrderProduct id - " + id);
    }
    
    return orderproduct;
  }

  @Override
  public void save(OrderProduct orderproduct) {
    orderproductRepository.save(orderproduct);
  }

  @Override
  public void deleteById(int id) {
    orderproductRepository.deleteById(id);
  }

  
}
