package com.watchshop.watchshop.OrderProduct;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.watchshop.watchshop.Order.Order;
import com.watchshop.watchshop.Product.Product;
import lombok.Data;

@Entity
@Table(name = "order_product")
@Data
public class OrderProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "order_product_id")
    private Integer orderProductId;
    
    @Column(name = "quantity")
    private Integer quantity;
    
    @Column(name = "total_price")
    private Integer totalPrice;
    
    @JoinColumn(name = "order_id", referencedColumnName = "order_id")
    @ManyToOne
    private Order order;
    
    @JoinColumn(name = "product_id", referencedColumnName = "product_id")
    @ManyToOne(optional = false)
    private Product product;

    public OrderProduct() {
    }

    public OrderProduct(Integer quantity, Integer totalPrice) {
        this.quantity = quantity;
        this.totalPrice = totalPrice;
    }
    
}
