package com.watchshop.watchshop.OrderProduct;

import java.util.List;

public interface OrderProductService {
  
  public List<OrderProduct> findAll();
  
  public OrderProduct findById(int id);
  
  public void save(OrderProduct orderproduct);
  
  public void deleteById(int id);
  
}
