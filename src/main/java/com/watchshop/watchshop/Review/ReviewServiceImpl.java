package com.watchshop.watchshop.Review;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReviewServiceImpl implements ReviewService {
  
  private ReviewRepository reviewRepository;

  @Autowired
  public ReviewServiceImpl(ReviewRepository theReviewRepository) {
    reviewRepository = theReviewRepository;
  }

  @Override
  public List<Review> findAll() {
    return reviewRepository.findAll();
  }

  @Override
  public Review findById(int id) {
    Optional<Review> result = reviewRepository.findById(id);
    
    Review review = null;
    
    if (result.isPresent()) {
      review = result.get();
    } else {
      throw new RuntimeException("Did not find Review id - " + id);
    }
    
    return review;
  }

  @Override
  public void save(Review Review) {
    reviewRepository.save(Review);
  }

  @Override
  public void deleteById(int id) {
    reviewRepository.deleteById(id);
  }
  
}
