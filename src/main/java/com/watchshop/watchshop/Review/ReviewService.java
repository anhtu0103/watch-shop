package com.watchshop.watchshop.Review;

import java.util.List;

public interface ReviewService {
  
  public List<Review> findAll();
  
  public Review findById(int id);
  
  public void save(Review review);
  
  public void deleteById(int id);
  
}
