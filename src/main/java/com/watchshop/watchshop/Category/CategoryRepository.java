package com.watchshop.watchshop.Category;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
  
  public List<Category> findAllByOrderByCreatedAtDesc();
}
