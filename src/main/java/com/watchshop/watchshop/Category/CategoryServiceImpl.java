package com.watchshop.watchshop.Category;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements CategoryService {
  
  private CategoryRepository categoryRepository;

  @Autowired
  public CategoryServiceImpl(CategoryRepository theCategoryRepository) {
    categoryRepository = theCategoryRepository;
  }

  @Override
  public List<Category> findAll() {
    return categoryRepository.findAllByOrderByCreatedAtDesc();
  }

  @Override
  public Category findById(int id) {
    Optional<Category> result = categoryRepository.findById(id);
    
    Category category = null;
    
    if (result.isPresent()) {
      category = result.get();
    } else {
      throw new RuntimeException("Did not find Category id - " + id);
    }
    
    return category;
  }

  @Override
  public void save(Category category) {
    categoryRepository.save(category);
  }

  @Override
  public void deleteById(int id) {
    categoryRepository.deleteById(id);
  }
  
}
