package com.watchshop.watchshop.Category;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.watchshop.watchshop.Admin.Admin;
import com.watchshop.watchshop.Admin.AdminService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CategoryController {

  @Autowired
  private AdminService adminService;
  
  @Autowired
  private CategoryService categoryService;

  @InitBinder
  public void initBinder(WebDataBinder dataBinder) {

    StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);

    dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
  }

  // FOR ADMIN
  
  @GetMapping("/admin/categories")
  public String categoryList(Model theModel) {
    List<Category> categoryList = categoryService.findAll();
    theModel.addAttribute("categoryList", categoryList);
    
    return "admin/categories/category_list";
  }
  
  @GetMapping("/admin/categories/showFormAddCategory")
  public String showFormAddCategory(Model theModel) {
    Category theCategory = new Category();
    
    theModel.addAttribute("category", theCategory);
    
    return "admin/categories/category_form";
  }
  
  @GetMapping("/admin/categories/showFormForUpdateCategory")
  public String showFormForUpdateCategory(@RequestParam("categoryId") int categoryId, Model model) {
    Category category = categoryService.findById(categoryId);
    model.addAttribute("category", category);
    
    return "admin/categories/category_form";
  }
  
  @GetMapping("/admin/categories/deleteCategory")
  public String deleteCategory(@RequestParam("categoryId") int categoryId) {
    categoryService.deleteById(categoryId);
    
    return "redirect:/admin/categories";
  }

  @PostMapping("/admin/categories/save")
  public String saveCategory(HttpServletRequest request, @ModelAttribute("category") Category theCategory) {
    Admin admin = adminService.findCurrentAdminLogin();
    theCategory.setCreatedBy(admin);
    categoryService.save(theCategory);
    
    return "redirect:/admin/categories";
  }
  
}
