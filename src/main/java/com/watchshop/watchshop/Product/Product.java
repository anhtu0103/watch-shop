package com.watchshop.watchshop.Product;

import java.time.LocalDateTime;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.watchshop.watchshop.Admin.Admin;
import com.watchshop.watchshop.CartItem.CartItem;
import com.watchshop.watchshop.Category.Category;
import com.watchshop.watchshop.OrderProduct.OrderProduct;
import com.watchshop.watchshop.Review.Review;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import lombok.Data;

@Entity
@Table(name = "product")
@Data
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "product_id")
    private Integer productId;
    
    @Basic(optional = false)
    @Column(name = "title")
    private String title;
    
    @Column(name = "description")
    private String description;
    
    @Basic(optional = false)
    @Column(name = "created_at")
    @CreationTimestamp
    private LocalDateTime createdAt;
    
    @Basic(optional = false)
    @Column(name = "updated_at")
    @UpdateTimestamp
    private LocalDateTime updatedAt;
    
    @Column(name = "image")
    private String image;
    
    @Basic(optional = false)
    @Column(name = "price")
    private int price;
    
    @JoinColumn(name = "created_by", referencedColumnName = "admin_id")
    @ManyToOne(optional = false)
    private Admin createdBy;
    
    @JoinColumn(name = "updated_by", referencedColumnName = "admin_id")
    @ManyToOne
    private Admin updatedBy;
    
    @JoinColumn(name = "category_id", referencedColumnName = "category_id", updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private Category category;
    @Column(name="category_id")
    private Integer categoryId;
    
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "product")
//    private Collection<Review> reviewCollection;
    
//    @OneToMany(mappedBy = "product")
//    private Collection<CartItem> cartItemCollection;
    
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "product")
//    private Collection<OrderProduct> orderProductCollection;

    public Product() {
    }

    public Product(String title, String description, String image, int price) {
        this.title = title;
        this.description = description;
        this.image = image;
        this.price = price;
    }

}
