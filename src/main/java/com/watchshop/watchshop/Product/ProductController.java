package com.watchshop.watchshop.Product;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.watchshop.watchshop.Admin.Admin;
import com.watchshop.watchshop.Admin.AdminService;
import com.watchshop.watchshop.Category.Category;
import com.watchshop.watchshop.Category.CategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProductController {

  @Autowired
  private AdminService adminService;
  
  @Autowired
  private ProductService productService;

  @Autowired
  private CategoryService categoryService;

  @InitBinder
  public void initBinder(WebDataBinder dataBinder) {

    StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);

    dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
  }

  // FOR ADMIN
  
  @GetMapping("/admin/products")
  public String productList(Model theModel) {
    List<Product> productList = productService.findAll();
    List<Category> categoryList = categoryService.findAll();
    theModel.addAttribute("productList", productList);
    theModel.addAttribute("categoryList", categoryList);
    return "admin/products/product_list";
  }
  
  @GetMapping("/admin/products/showFormAddProduct")
  public String showFormAddProduct(Model theModel) {
    Product theProduct = new Product();
    theModel.addAttribute("product", theProduct);
    theModel.addAttribute("categoryList", categoryService.findAll());
    return "admin/products/product_form";
  }
  
  @GetMapping("/admin/products/showFormForUpdateProduct")
  public String showFormForUpdateProduct(@RequestParam("productId") int productId, Model model) {
    Product product = productService.findById(productId);
    
    model.addAttribute("product", product);
    model.addAttribute("categoryList", categoryService.findAll());
    
    return "admin/products/product_form";
  }
  
  @GetMapping("/admin/products/deleteProduct")
  public String deleteProduct(@RequestParam("productId") int productId) {
    productService.deleteById(productId);
    return "redirect:/admin/products";
  }

  @PostMapping("/admin/products/save")
  public String saveProduct(HttpServletRequest request, @ModelAttribute("product") Product theProduct) {
    Admin admin = adminService.findCurrentAdminLogin();
    theProduct.setCreatedBy(admin);
    productService.save(theProduct);
    return "redirect:/admin/products";
  }
  
}
