package com.watchshop.watchshop.Product;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {
  
  private ProductRepository productRepository;

  @Autowired
  public ProductServiceImpl(ProductRepository theProductRepository) {
    productRepository = theProductRepository;
  }

  @Override
  public List<Product> findAll() {
    return productRepository.findAll();
  }

  @Override
  public Product findById(int id) {
    Optional<Product> result = productRepository.findById(id);
    
    Product product = null;
    
    if (result.isPresent()) {
      product = result.get();
    } else {
      throw new RuntimeException("Did not find Product id - " + id);
    }
    
    return product;
  }

  @Override
  public void save(Product Product) {
    productRepository.save(Product);
  }

  @Override
  public void deleteById(int id) {
    productRepository.deleteById(id);
  }
  
}
