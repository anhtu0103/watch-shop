package com.watchshop.watchshop.ShoppingCart;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {
  
  private ShoppingCartRepository shoppingCartRepository;

  @Autowired
  public ShoppingCartServiceImpl(ShoppingCartRepository theShoppingCartRepository) {
    shoppingCartRepository = theShoppingCartRepository;
  }

  @Override
  public List<ShoppingCart> findAll() {
    return shoppingCartRepository.findAll();
  }

  @Override
  public ShoppingCart findById(int id) {
    Optional<ShoppingCart> result = shoppingCartRepository.findById(id);
    
    ShoppingCart shoppingCart = null;
    
    if (result.isPresent()) {
      shoppingCart = result.get();
    } else {
      throw new RuntimeException("Did not find ShoppingCart id - " + id);
    }
    
    return shoppingCart;
  }

  @Override
  public void save(ShoppingCart ShoppingCart) {
    shoppingCartRepository.save(ShoppingCart);
  }

  @Override
  public void deleteById(int id) {
    shoppingCartRepository.deleteById(id);
  }
  
}
