package com.watchshop.watchshop.ShoppingCart;

import java.time.LocalDateTime;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.watchshop.watchshop.CartItem.CartItem;
import com.watchshop.watchshop.Customer.Customer;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import lombok.Data;

@Entity
@Table(name = "shopping_cart")
@Data
public class ShoppingCart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "shopping_cart_id")
    private Integer shoppingCartId;
    
    @Column(name = "quantity")
    private Integer quantity;
    
    @Column(name = "total_price")
    private Integer totalPrice;
    
    @Column(name = "created_at")
    @CreationTimestamp
    private LocalDateTime createdAt;
    
    @Column(name = "updated_at")
    @UpdateTimestamp
    private LocalDateTime updatedAt;
    
    @JoinColumn(name = "customer_id", referencedColumnName = "customer_id")
    @ManyToOne
    private Customer customer;
    
    // @OneToMany(cascade = CascadeType.ALL, mappedBy = "shoppingCart")
    // private Collection<CartItem> cartItemCollection;

    public ShoppingCart() {
    }

    public ShoppingCart(Integer quantity, Integer totalPrice) {
        this.quantity = quantity;
        this.totalPrice = totalPrice;
    }
    
}
