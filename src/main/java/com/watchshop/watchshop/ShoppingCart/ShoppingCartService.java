package com.watchshop.watchshop.ShoppingCart;

import java.util.List;

public interface ShoppingCartService {
  
  public List<ShoppingCart> findAll();
  
  public ShoppingCart findById(int id);
  
  public void save(ShoppingCart shoppingCart);
  
  public void deleteById(int id);
  
}
