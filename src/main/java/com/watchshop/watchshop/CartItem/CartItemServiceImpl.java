package com.watchshop.watchshop.CartItem;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartItemServiceImpl implements CartItemService {
  
  private CartItemRepository cartItemRepository;

  @Autowired
  public CartItemServiceImpl(CartItemRepository theCartItemRepository) {
    cartItemRepository = theCartItemRepository;
  }

  @Override
  public List<CartItem> findAll() {
    return cartItemRepository.findAll();
  }

  @Override
  public CartItem findById(int id) {
    Optional<CartItem> result = cartItemRepository.findById(id);
    
    CartItem cartItem = null;
    
    if (result.isPresent()) {
      cartItem = result.get();
    } else {
      throw new RuntimeException("Did not find CartItem id - " + id);
    }
    
    return cartItem;
  }

  @Override
  public void save(CartItem CartItem) {
    cartItemRepository.save(CartItem);
  }

  @Override
  public void deleteById(int id) {
    cartItemRepository.deleteById(id);
  }
  
}
