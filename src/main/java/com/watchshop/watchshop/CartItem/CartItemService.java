package com.watchshop.watchshop.CartItem;

import java.util.List;

public interface CartItemService {
  
  public List<CartItem> findAll();
  
  public CartItem findById(int id);
  
  public void save(CartItem cartItem);
  
  public void deleteById(int id);
  
}
