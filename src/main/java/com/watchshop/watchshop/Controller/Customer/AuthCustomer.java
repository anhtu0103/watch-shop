package com.watchshop.watchshop.Controller.Customer;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/customer")
public class AuthCustomer {

  @CrossOrigin
  @GetMapping("/showLoginCustomer")
  public String showLoginCustomer() {

    return "client/login";

  }

}
