package com.watchshop.watchshop.Controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.watchshop.watchshop.CRM.CrmUser;
import com.watchshop.watchshop.CartItem.CartItem;
import com.watchshop.watchshop.Category.CategoryService;
import com.watchshop.watchshop.Customer.Customer;
import com.watchshop.watchshop.Customer.CustomerService;
import com.watchshop.watchshop.Order.Order;
import com.watchshop.watchshop.Order.OrderService;
import com.watchshop.watchshop.OrderProduct.OrderProduct;
import com.watchshop.watchshop.OrderProduct.OrderProductService;
import com.watchshop.watchshop.Product.Product;
import com.watchshop.watchshop.Product.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class ClientController {

  @Autowired
  private CustomerService customerService;
  
  @Autowired
  private OrderProductService orderProductService;
  
  @Autowired
  private OrderService orderService;

  @Autowired
  private CategoryService categoryService;

  @Autowired
  private ProductService productService;

  @InitBinder
  public void initBinder(WebDataBinder dataBinder) {

    StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);

    dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
  }

  @GetMapping("/checkout")
  public String checkout(Model theModel) {

    // theModel.addAttribute("crmUser", new CrmUser());

    return "client/checkout";
  }

  @GetMapping("/register")
  public String showMyLoginPage(Model theModel) {

    theModel.addAttribute("crmUser", new CrmUser());

    return "client/register";
  }

  @PostMapping("/customer/updateProfileCustomer")
  public String updateProfileCustomer(@ModelAttribute("crmUser") CrmUser crmUser, HttpServletRequest request) {
    Customer customer = customerService.findById(crmUser.getCustomerId());
    customer.setAddress(crmUser.getAddress());
    customer.setName(crmUser.getName());
    customerService.save(customer);
    request.getSession().setAttribute("user", customer);

    return "redirect:/";
  }

  @GetMapping("/customer/profile")
  public String profile(@RequestParam("customerId") int customerId, Model model) {

    Customer customer = customerService.findById(customerId);
    CrmUser tempCrmUser = new CrmUser();
    tempCrmUser.setCustomerId(customer.getCustomerId());
    tempCrmUser.setName(customer.getName());
    tempCrmUser.setAddress(customer.getAddress());
    tempCrmUser.setEmail(customer.getEmail());
    model.addAttribute("crmUser", tempCrmUser);

    return "client/profile";
  }

  @PostMapping("/processRegistrationForm")
  public String processRegistrationForm(@Valid @ModelAttribute("crmUser") CrmUser theCrmUser,
      BindingResult theBindingResult, Model theModel) {
    String email = theCrmUser.getEmail();
    log.info("Processing registration form for: " + email);

    // if (theBindingResult.hasErrors()) {
    // log.warn("ERROR HERE");
    // return "registration-form-admin";
    // }

    Customer existing = customerService.findByEmail(email);

    if (existing != null) {
      theModel.addAttribute("crmUser", new CrmUser());
      theModel.addAttribute("registrationError", "Email already exists.");

      log.warn("Email already exists.");
      return "registration-form-admin";
    }
    log.warn("theCrmUser" + theCrmUser);
    customerService.saveViaCRMUser(theCrmUser);

    log.info("Successfully created user: " + email);

    return "client/index";
  }

  @GetMapping("")
  public String showCustomerIndex(Model theModel) {
    theModel.addAttribute("categoryList", categoryService.findAll());
    theModel.addAttribute("productList", productService.findAll());
    return "client/index";
  }

  @GetMapping("/contact")
  public String contact(Model theModel) {
    return "client/contact";
  }

  @GetMapping("/products")
  public String productDetail(@RequestParam("productId") int productId, Model model) {
    model.addAttribute("product", productService.findById(productId));
    return "client/single";
  }

  @GetMapping("/customer/showCheckout")
  public String showCheckout() {
    return "client/checkout";
  }

  @GetMapping("/customer/remove-cart-item")
  public String removeCartItem(@RequestParam("productId") int productId, HttpServletRequest request) {
    HashMap<Integer, CartItem> cart = (HashMap<Integer, CartItem>) request.getSession().getAttribute("cart");
    cart.remove(productId);
    request.getSession().setAttribute("cart", cart);
    return "client/checkout";
  }

  @GetMapping("/customer/checkout")
  public String checkout(HttpServletRequest request) {
    Customer customer = (Customer) request.getSession().getAttribute("user");
    Order order = new Order();
    order.setTotalPrice(0);
    order.setCustomer(customer);
    order.setStatus("pending");
    orderService.save(order);
    Map<Integer, CartItem> cart = (Map<Integer, CartItem>) request.getSession().getAttribute("cart");
    
    cart.forEach((productId, cartItem) -> {
      Product product = productService.findById(productId);
      OrderProduct orderProduct = new OrderProduct();
      orderProduct.setOrder(order);
      orderProduct.setProduct(product);
      orderProduct.setQuantity(cartItem.getQuantity());
      orderProduct.setTotalPrice(cartItem.getQuantity() * cartItem.getProduct().getPrice());
      
      order.setTotalPrice(order.getTotalPrice() + orderProduct.getTotalPrice());
      orderProductService.save(orderProduct);
    });
    
    orderService.save(order);
    request.getSession().removeAttribute("cart");
    return "client/index";
  }

  @GetMapping("/customer/add-to-cart")
  public String addToCart(@RequestParam("productId") int productId, Model model, HttpServletRequest request) {
    Product product = productService.findById(productId);
    if (request.getSession().getAttribute("cart") == null) {
      HashMap<Integer, CartItem> cart = new HashMap<Integer, CartItem>();
      CartItem cartItem = new CartItem();
      cartItem.setProduct(product);
      cartItem.setQuantity(1);
      cartItem.setTotalPrice(product.getPrice() * cartItem.getQuantity());
      cart.put(productId, cartItem);
      request.getSession().setAttribute("cart", cart);
    } else {
      HashMap<Integer, CartItem> cart = (HashMap<Integer, CartItem>) request.getSession().getAttribute("cart");
      if (cart.containsKey(productId)) {
        CartItem cartItem = cart.get(productId);
        cartItem.setQuantity(cartItem.getQuantity() + 1);
        cartItem.setTotalPrice(product.getPrice() * cartItem.getQuantity());
        cart.put(productId, cartItem);
        request.getSession().setAttribute("cart", cart);
      } else {
        CartItem cartItem = new CartItem();
        cartItem.setProduct(product);
        cartItem.setQuantity(1);
        cartItem.setTotalPrice(product.getPrice() * cartItem.getQuantity());
        cart.put(productId, cartItem);
        request.getSession().setAttribute("cart", cart);
      }
    }
    model.addAttribute("product", productService.findById(productId));
    return "redirect:/products?productId=" + productId;
  }

  @GetMapping("/login")
  public String login(HttpServletRequest request) {
    return "customer/login";
  }

  @GetMapping("/customer/orders")
  public String customerOrders(@RequestParam("customerId") int customerId, Model model) {
    List<OrderProduct> orderProductList = orderProductService.findAll();
    ArrayList<OrderProduct> result = new ArrayList<OrderProduct>();
    orderProductList.forEach(orderProduct -> {
      if (orderProduct.getOrder().getCustomer().getCustomerId() == customerId) {
        result.add(orderProduct);
      }
    });
    model.addAttribute("orderProductList", result);

    return "client/order-history";
  }
}
