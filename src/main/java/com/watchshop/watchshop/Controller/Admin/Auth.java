package com.watchshop.watchshop.Controller.Admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class Auth {

  @CrossOrigin
  @GetMapping("/showLoginAdmin")
  public String showLoginAdmin() {

    return "admin/login";

  }

}
