package com.watchshop.watchshop.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;

import com.watchshop.watchshop.Admin.Admin;
import com.watchshop.watchshop.Admin.AdminService;
import com.watchshop.watchshop.CRM.CrmUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/admin")
@Slf4j
public class AdminController {

  @Autowired
  private AdminService adminService;

  @InitBinder
  public void initBinder(WebDataBinder dataBinder) {

    StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);

    dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
  }
  
  @PostMapping("/changepassword")
  public String changePasswordAdmin(@ModelAttribute("admin") Admin tempAdmin) {
    Admin admin = adminService.findById(tempAdmin.getAdminId());
    String newPassword = adminService.hashPassword(tempAdmin.getPassword());
    admin.setPassword(newPassword);
    adminService.save(admin);
    return "redirect:/admin";
  }
  
  @GetMapping("/showFormChangePasswordAdmin")
  public String showFormChangePasswordAdmin(@RequestParam("adminId") int adminId, Model model) {
    log.info("🔥 id " + adminId);
    Admin admin = adminService.findById(adminId);
    log.info("🔥 admin " + admin);
    model.addAttribute("admin", admin);

    return "admin/admin/password-form-admin";
  }
  
  @GetMapping("/showRegistrationForm")
  public String showMyLoginPage(Model theModel) {

    theModel.addAttribute("crmUser", new CrmUser());

    return "registration-form-admin";
  }

  @PostMapping("/processRegistrationForm")
  public String processRegistrationForm(@Valid @ModelAttribute("crmUser") CrmUser theCrmUser,
      BindingResult theBindingResult, Model theModel) {
    String email = theCrmUser.getEmail();
    log.info("Processing registration form for: " + email);

    if (theBindingResult.hasErrors()) {
      return "registration-form-admin";
    }

    // check the database if user already exists
    Admin existing = adminService.findByEmail(email);

    if (existing != null) {
      theModel.addAttribute("crmUser", new CrmUser());
      theModel.addAttribute("registrationError", "Email already exists.");

      log.warn("Email already exists.");
      return "registration-form-admin";
    }

    adminService.saveViaCRMUser(theCrmUser);

    log.info("Successfully created user: " + email);

    return "registration-confirmation-admin";
  }

  @GetMapping("")
  public String showAdmin(HttpServletRequest request) {
    return "admin/index";
  }

  @GetMapping("/form_component")
  public String form_component(HttpServletRequest request) {
    return "admin/form_component";
  }

  @GetMapping("/form_validation")
  public String form_validation(HttpServletRequest request) {
    return "admin/form_validation";
  }

  @GetMapping("/general")
  public String general(HttpServletRequest request) {
    return "admin/general";
  }

  @GetMapping("/buttons")
  public String buttons(HttpServletRequest request) {
    return "admin/buttons";
  }

  @GetMapping("/grids")
  public String grids(HttpServletRequest request) {
    return "admin/grids";
  }

  @GetMapping("/widgets")
  public String widgets(HttpServletRequest request) {
    return "admin/widgets";
  }

  @GetMapping("/chart-chartjs")
  public String chart(HttpServletRequest request) {
    return "admin/chart-chartjs";
  }

  @GetMapping("/basic_table")
  public String table(HttpServletRequest request) {
    return "admin/basic_table";
  }
  
  @GetMapping("/product_list")
  public String productList(HttpServletRequest request) {
    return "admin/product_list";
  }

  @GetMapping("/profile")
  public String profile(HttpServletRequest request) {
    return "admin/profile";
  }

  @GetMapping("/login")
  public String login(HttpServletRequest request) {
    return "admin/login";
  }

  @GetMapping("/contact")
  public String contact(HttpServletRequest request) {
    return "admin/contact";
  }

  @GetMapping("/blank")
  public String blank(HttpServletRequest request) {
    return "admin/blank";
  }

  @GetMapping("/404")
  public String notFound(HttpServletRequest request) {
    return "admin/404";
  }

}
