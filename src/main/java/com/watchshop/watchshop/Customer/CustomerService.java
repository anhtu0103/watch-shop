package com.watchshop.watchshop.Customer;

import java.util.List;

import com.watchshop.watchshop.CRM.CrmUser;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface CustomerService extends UserDetailsService {
  
  public List<Customer> findAll();
  
  public Customer findById(int id);
  
  public void save(Customer customer);
  
  public void deleteById(int id);
  
  public Customer findByEmail(String email);
  
  public Customer findCurrentCustomerLogin();

	public void saveViaCRMUser(CrmUser crmCustomer);
  
}
