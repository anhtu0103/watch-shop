package com.watchshop.watchshop.Customer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.watchshop.watchshop.CRM.CrmUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CustomerServiceImpl implements CustomerService {

  private CustomerRepository customerRepository;

  @Autowired
  @Qualifier("passwordEncoderCustomer")
  private BCryptPasswordEncoder passwordEncoder;

  @Autowired
  public CustomerServiceImpl(CustomerRepository theCustomerRepository) {
    customerRepository = theCustomerRepository;

  }

  @Override
  public List<Customer> findAll() {
    return customerRepository.findAll();
  }

  @Override
  public Customer findById(int id) {
    Optional<Customer> result = customerRepository.findById(id);

    Customer customer = null;

    if (result.isPresent()) {
      customer = result.get();
    } else {
      throw new RuntimeException("Did not find customer id - " + id);
    }

    return customer;
  }

  @Override
  public Customer findByEmail(String email) {
    Customer customer = customerRepository.findByEmail(email);

    return customer;
  }

  @Override
  public void save(Customer customer) {
    customerRepository.save(customer);
  }

  @Override
  public void deleteById(int id) {
    customerRepository.deleteCustomer(id);
  }

  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    Customer customer = customerRepository.findByEmail(email);
    if (customer == null) {
      throw new UsernameNotFoundException("Invalid username or password");
    }
    List<GrantedAuthority> authorities = new ArrayList<>();
    return new org.springframework.security.core.userdetails.User(customer.getEmail(), customer.getPassword(), authorities);
  }

  @Override
  public void saveViaCRMUser(CrmUser crmCustomer) {
    Customer customer = new Customer();
    customer.setEmail(crmCustomer.getEmail());
    customer.setName(crmCustomer.getName());
    customer.setAddress(crmCustomer.getAddress());
    customer.setPhone(crmCustomer.getPhone());
    customer.setPassword(passwordEncoder.encode(crmCustomer.getPassword()));
    customerRepository.save(customer);
  }

  @Override
  public Customer findCurrentCustomerLogin() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    Customer customer = customerRepository.findByEmail(auth.getName());
    return customer;
  }

}
