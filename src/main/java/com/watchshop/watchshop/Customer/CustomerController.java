package com.watchshop.watchshop.Customer;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import com.watchshop.watchshop.Admin.Admin;
import com.watchshop.watchshop.Admin.AdminService;
import com.watchshop.watchshop.CRM.CrmUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class CustomerController {

  @Autowired
  private AdminService adminService;

  @Autowired
  private CustomerService customerService;

  @InitBinder
  public void initBinder(WebDataBinder dataBinder) {

    StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);

    dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
  }

  // FOR ADMIN

  @GetMapping("/admin/customers")
  public String customerList(Model theModel) {
    List<Customer> customerList = customerService.findAll();
    theModel.addAttribute("customerList", customerList);

    return "admin/customers/customer_list";
  }

  @GetMapping("/admin/customers/showFormAddCustomer")
  public String showFormAddCustomer(Model theModel) {
    Customer theCustomer = new Customer();

    theModel.addAttribute("customer", theCustomer);

    return "admin/customers/customer_form";
  }

  @GetMapping("/admin/customers/showFormForUpdateCustomer")
  public String showFormForUpdateCustomer(@RequestParam("customerId") int customerId, Model model) {
    Customer customer = customerService.findById(customerId);
    CrmUser crmUser = new CrmUser();
    crmUser.setCustomerId(customer.getCustomerId());
    crmUser.setAddress(customer.getAddress());
    crmUser.setName(customer.getName());
    crmUser.setEmail(customer.getEmail());
    model.addAttribute("customer", crmUser);

    return "admin/customers/customer_form";
  }

  @GetMapping("/admin/customers/deleteCustomer")
  public String deleteCustomer(@RequestParam("customerId") int customerId) {
    customerService.deleteById(customerId);

    return "redirect:/admin/customers";
  }

  @Transactional
  @PostMapping("/admin/customers/save")
  public String saveCustomer(HttpServletRequest request, @ModelAttribute("customer") CrmUser crmUser) {
    Customer customer = customerService.findById(crmUser.getCustomerId());
    customer.setName(crmUser.getName());
    customer.setAddress(crmUser.getAddress());
    log.info("admin/customers/save: start");
    log.info("admin/customers/save: customer " + customer);
    customerService.save(customer);
    log.info("admin/customers/save: end"); 

    return "redirect:/admin/customers";
  }

}
