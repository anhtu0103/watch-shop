package com.watchshop.watchshop.Customer;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
  @Query("SELECT c FROM Customer c WHERE c.email = ?1")
  Customer findByEmail(String email);

  @Transactional
  @Modifying
  @Query("delete from Customer c where c.customerId =:#{#customerId}")
  void deleteCustomer(@Param("customerId") Integer customerId);
}