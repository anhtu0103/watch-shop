package com.watchshop.watchshop.Order;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import com.watchshop.watchshop.Admin.Admin;
import com.watchshop.watchshop.Admin.AdminService;
import com.watchshop.watchshop.CRM.CrmUser;
import com.watchshop.watchshop.Customer.Customer;
import com.watchshop.watchshop.Customer.CustomerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class OrderController {

  @Autowired
  private OrderService orderService;
  @Autowired
  private CustomerService customerService;

  @InitBinder
  public void initBinder(WebDataBinder dataBinder) {

    StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);

    dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
  }

  // FOR ADMIN

  @GetMapping("/admin/orders")
  public String orderList(Model theModel) {
    List<Order> orderList = orderService.findByOrderByCreatedAtDesc();
    theModel.addAttribute("orderList", orderList);

    return "admin/orders/orders_list";
  }

  @GetMapping("/admin/orders/showFormForUpdateOrder")
  public String showFormForUpdateCustomer(@RequestParam("orderId") int orderId, Model model) {
    Order order = orderService.findById(orderId);
    model.addAttribute("order", order);

    return "admin/orders/order_form";
  }

  @Transactional
  @PostMapping("/admin/orders/save")
  public String saveOrder(HttpServletRequest request, @ModelAttribute("order") Order order) {
    log.info("here " + order);
    Customer customer = customerService.findById(order.getCustomer().getCustomerId());
    order.setCustomer(customer);
    orderService.save(order);

    return "redirect:/admin/orders";
  }

}
