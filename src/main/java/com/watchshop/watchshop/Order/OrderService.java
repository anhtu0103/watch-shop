package com.watchshop.watchshop.Order;

import java.util.List;

public interface OrderService {
  
  public List<Order> findAll();
  
  public Order findById(int id);
  
  public void save(Order order);
  
  public void deleteById(int id);
  
  public List<Order> findByOrderByCreatedAtDesc();
  
}
