package com.watchshop.watchshop.Order;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {
  
  private OrderRepository orderRepository;

  @Autowired
  public OrderServiceImpl(OrderRepository theorderRepository) {
    orderRepository = theorderRepository;
  }

  @Override
  public List<Order> findAll() {
    return orderRepository.findAll();
  }

  @Override
  public Order findById(int id) {
    Optional<Order> result = orderRepository.findById(id);
    
    Order order = null;
    
    if (result.isPresent()) {
      order = result.get();
    } else {
      throw new RuntimeException("Did not find Order id - " + id);
    }
    
    return order;
  }

  @Override
  public void save(Order Order) {
    orderRepository.save(Order);
  }

  @Override
  public void deleteById(int id) {
    orderRepository.deleteById(id);
  }

  @Override
  public List<Order> findByOrderByCreatedAtDesc() {
    return orderRepository.findByOrderByCreatedAtDesc();
  }
  
}
