package com.watchshop.watchshop.Order;

import java.time.LocalDateTime;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.watchshop.watchshop.Customer.Customer;
import com.watchshop.watchshop.OrderProduct.OrderProduct;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import lombok.Data;

@Entity
@Table(name = "tb_order")
@Data
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "order_id")
    private Integer orderId;

    @Column(name = "status")
    private String status;

    @Basic(optional = false)
    @Column(name = "created_at")
    @CreationTimestamp
    private LocalDateTime createdAt;

    @Basic(optional = false)
    @Column(name = "updated_at")
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @Column(name = "note")
    private String note;

    @Column(name = "total_price")
    private Integer totalPrice;

    @JoinColumn(name = "customer_id", referencedColumnName = "customer_id")
    @ManyToOne(optional = false)
    private Customer customer;

    @OneToMany(mappedBy = "order")
    private Collection<OrderProduct> orderProductCollection;

    public Order() {
    }

    public Order(String status, String note, Integer totalPrice) {
        this.status = status;
        this.note = note;
        this.totalPrice = totalPrice;
    }

}
