package com.watchshop.watchshop.Order;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Integer> {
  List<Order> findByOrderByCreatedAtDesc();
}
