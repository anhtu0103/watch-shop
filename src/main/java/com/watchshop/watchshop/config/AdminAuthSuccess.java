package com.watchshop.watchshop.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.watchshop.watchshop.Admin.Admin;
import com.watchshop.watchshop.Admin.AdminService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class AdminAuthSuccess implements AuthenticationSuccessHandler {
  
  @Autowired
  private AdminService adminService;
  
  @Override
  @Transactional
  public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
      Authentication authentication) throws IOException, ServletException {
    String email = authentication.getName();
    Admin theAdmin = adminService.findByEmail(email);
    HttpSession session = request.getSession();
    session.setAttribute("admin", theAdmin);
    response.sendRedirect(request.getContextPath() + "/admin/");

  }

}
