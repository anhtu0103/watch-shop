package com.watchshop.watchshop.config;

import com.watchshop.watchshop.Admin.AdminService;
import com.watchshop.watchshop.Customer.CustomerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class MultiHttpSecurityConfig {

  @Configuration
  @Order(1)
  public static class AdminSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AdminService adminsService;

    @Autowired
    private AdminAuthSuccess adminAuthSuccess;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
      auth.authenticationProvider(authenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

      http
          .antMatcher("/admin/**").authorizeRequests()
          .antMatchers("/admin/css/**").permitAll()
          .antMatchers("/admin/img/**").permitAll()
          .antMatchers("/admin/fonts/**").permitAll()
          .antMatchers("/admin/**").authenticated()
          .and()
          .formLogin()
            .loginPage("/admin/showLoginAdmin")
            .loginProcessingUrl("/admin/authenticateAdmin")
            .successHandler(adminAuthSuccess).permitAll()
          .and()
          .logout().logoutUrl("/admin/logout").permitAll()
          .and()
          .exceptionHandling()
          .accessDeniedPage("/access-denied");
    }

    public DaoAuthenticationProvider authenticationProvider() {
      DaoAuthenticationProvider auth = new DaoAuthenticationProvider();
      auth.setUserDetailsService(adminsService);
      auth.setPasswordEncoder(passwordEncoder());
      return auth;
    }

    @Bean(name = "passwordEncoderAdmin")
    public BCryptPasswordEncoder passwordEncoder() {
      return new BCryptPasswordEncoder();
    }
  }

  @Configuration
  @Order(2)
  public static class CustomerSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CustomerAuthSuccess customerAuthSuccess;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
      auth.authenticationProvider(authenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

      http.authorizeRequests()
          // .antMatcher("/customer/**").authorizeRequests()
          .antMatchers("/admin/css/**").permitAll()
          .antMatchers("/admin/img/**").permitAll()
          .antMatchers("/admin/fonts/**").permitAll()
          .antMatchers("/customer/**").authenticated()
          .anyRequest().permitAll()
          .and()
          .formLogin()
            .loginPage("/customer/showLoginCustomer")
            .loginProcessingUrl("/customer/authenticateCustomer")
            .successHandler(customerAuthSuccess).permitAll()
          .and()
          .logout().logoutSuccessUrl("/").permitAll()
          .and()
          .exceptionHandling()
          .accessDeniedPage("/access-denied");
    }

    public DaoAuthenticationProvider authenticationProvider() {
      DaoAuthenticationProvider auth = new DaoAuthenticationProvider();
      auth.setUserDetailsService(customerService);
      auth.setPasswordEncoder(passwordEncoder());
      return auth;
    }

    @Bean(name = "passwordEncoderCustomer")
    public BCryptPasswordEncoder passwordEncoder() {
      return new BCryptPasswordEncoder();
    }
  }

}
