package com.watchshop.watchshop.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.watchshop.watchshop.Customer.Customer;
import com.watchshop.watchshop.Customer.CustomerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component
public class CustomerAuthSuccess implements AuthenticationSuccessHandler {
  
  @Autowired
  private CustomerService customerService;
  
  @Override
  @Transactional
  public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
      Authentication authentication) throws IOException, ServletException {
    String email = authentication.getName();
    Customer theCustomer = customerService.findByEmail(email);
    HttpSession session = request.getSession();
		session.setAttribute("user", theCustomer);
    response.sendRedirect(request.getContextPath() + "/");

  }

}
