package com.watchshop.watchshop.Admin;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AdminRepository extends JpaRepository<Admin, Integer> {

  @Query("SELECT a FROM Admin a WHERE a.email = ?1")
  Admin findByEmail(String email);

}
