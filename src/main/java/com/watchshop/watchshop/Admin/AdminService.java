package com.watchshop.watchshop.Admin;

import java.util.List;

import com.watchshop.watchshop.CRM.CrmUser;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface AdminService extends UserDetailsService {

  public List<Admin> findAll();

  public Admin findById(int id);

  public void save(Admin admin);

  public void deleteById(int id);

  public Admin findByEmail(String email);
  
  public Admin findCurrentAdminLogin();

  public void saveViaCRMUser(CrmUser crmAdmin);
  
  public String hashPassword(String password);

}
