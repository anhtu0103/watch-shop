package com.watchshop.watchshop.Admin;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import com.watchshop.watchshop.CRM.CrmUser;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.security.core.userdetails.User;

@Service
public class AdminServiceImpl implements AdminService {

  private AdminRepository adminRepository;

  @Autowired
  @Qualifier("passwordEncoderAdmin")
  private BCryptPasswordEncoder passwordEncoder;

  @Autowired
  public AdminServiceImpl(AdminRepository theAdminRepository) {
    adminRepository = theAdminRepository;
  }

  @Override
  public List<Admin> findAll() {
    return adminRepository.findAll();
  }

  @Override
  public Admin findById(int id) {
    Optional<Admin> result = adminRepository.findById(id);

    Admin admin = null;

    if (result.isPresent()) {
      admin = result.get();
    } else {
      throw new RuntimeException("Did not find admin id - " + id);
    }

    return admin;
  }

  @Override
  public void save(Admin admin) {
    adminRepository.save(admin);
  }

  @Override
  public void deleteById(int id) {
    adminRepository.deleteById(id);
  }

  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    Admin admin = adminRepository.findByEmail(email);
    if (admin == null) {
      throw new UsernameNotFoundException("Invalid username or password");
    }
    List<GrantedAuthority> authorities = new ArrayList<>();
    return new User(admin.getEmail(), admin.getPassword(), authorities);
  }

  @Override
  public void saveViaCRMUser(CrmUser crmAdmin) {
    Admin admin = new Admin();
    admin.setEmail(crmAdmin.getEmail());
    admin.setName(crmAdmin.getName());
    String pass = passwordEncoder.encode(crmAdmin.getPassword());
    admin.setPassword(pass);
    adminRepository.save(admin);
  }

  @Override
  public Admin findByEmail(String email) {
    Admin admin = adminRepository.findByEmail(email);

    return admin;
  }

  @Override
  public Admin findCurrentAdminLogin() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    Admin admin = adminRepository.findByEmail(auth.getName());
    return admin;
  }

  @Override
  public String hashPassword(String password) {
    return passwordEncoder.encode(password);
  }

}
