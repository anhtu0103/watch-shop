package com.watchshop.watchshop.Admin;

import java.time.LocalDateTime;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.watchshop.watchshop.Category.Category;
import com.watchshop.watchshop.Customer.Customer;
import com.watchshop.watchshop.Product.Product;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import lombok.Data;

@Entity
@Table(name = "admin")
@Data
public class Admin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "admin_id")
    private Integer adminId;

    @Basic(optional = false)
    @Column(name = "email")
    private String email;

    @Column(name = "name")
    private String name;

    @Basic(optional = false)
    @Column(name = "password")
    private String password;

    @Column(name = "address")
    private String address;

    @Column(name = "phone")
    private String phone;

    @Basic(optional = false)
    @Column(name = "created_at")
    @CreationTimestamp
    private LocalDateTime createdAt;

    @Basic(optional = false)
    @Column(name = "updated_at")
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @Column(name = "created_by")
    private Integer createdBy;

    @Column(name = "updated_by")
    private String updatedBy;

    // @OneToMany(cascade = CascadeType.ALL, mappedBy = "createdBy")
    // private Collection<Product> productCollection;

    // @OneToMany(cascade = CascadeType.ALL, mappedBy = "createdBy")
    // private Collection<Category> categoryCollection;

    // @OneToMany(mappedBy = "createdBy")
    // private Collection<Customer> customerCollection;

    public Admin() {
    }

    public Admin(String email, String name, String password, String address, String phone) {
        this.email = email;
        this.name = name;
        this.password = password;
        this.address = address;
        this.phone = phone;
    }

}
